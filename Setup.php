<?php

/**
 *  ▄▄▄·  ▄▄▄· ▄▄▄· ▄▄▄▄▄ ▄ .▄ ▄· ▄▌
 * ▐█ ▀█ ▐█ ▄█▐█ ▀█ •██  ██▪▐█▐█▪██▌
 * ▄█▀▀█  ██▀·▄█▀▀█  ▐█.▪██▀▐█▐█▌▐█▪
 * ▐█ ▪▐▌▐█▪·•▐█ ▪▐▌ ▐█▌·██▌▐▀ ▐█▀·.
 *  ▀  ▀ .▀    ▀  ▀  ▀▀▀ ▀▀▀ ·  ▀ •
 *  <https://fortreeforums.xyz/>
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] Activity/Longevity Meters ("ActLong").
 *
 *  ActLong is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ActLong is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ActLong.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\ActLong;

use XF\AddOn\AbstractSetup;
use XF\AddOn\StepRunnerInstallTrait;
use XF\AddOn\StepRunnerUninstallTrait;
use XF\AddOn\StepRunnerUpgradeTrait;

class Setup extends AbstractSetup
{
    use StepRunnerInstallTrait;
    use StepRunnerUpgradeTrait;
    use StepRunnerUninstallTrait;

    use Install\Table;

    use Install\Upgrade1010070;
    use Install\Upgrade1020070;
    use Install\Upgrade1020170;
	
    public function installStep1()
    {
        $schemaManager = $this->schemaManager();
        $tables = $this->getAlteredTables();

        foreach($tables as $name => $definition)
        {
            if($schemaManager->tableExists($name))
            {
                $schemaManager->alterTable($name, $definition);
            }
        }
    }
	
    public function uninstallStep1()
    {
        $schemaManager = $this->schemaManager();
        $tables = $this->dropAlteredTableColumns();

        foreach($tables as $name => $definition)
        {
            $schemaManager->alterTable($name, $definition);
        }
    }
	
    /**
     * Fetch User ID 1's registration date and store it in SimpleCache.
     * This will be used as a "start date" for the forum, and Longevity meters are
     * based around this value.
     */
    public function installStep2()
    {
        $app = $this->app;
        
        $simpleCache = $app->simpleCache();
        $date = $app->em()->find('XF:User', 1)->register_date;
		   
        $simpleCache['apathy/ActLong']['forum_start'] = $date;
    }
	
    public function uninstallStep2()
    {
        unset($this->app->simpleCache()['apathy/ActLong']['forum_start']);		
    }
}
