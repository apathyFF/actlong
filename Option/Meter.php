<?php

/**
 *  ▄▄▄·  ▄▄▄· ▄▄▄· ▄▄▄▄▄ ▄ .▄ ▄· ▄▌
 * ▐█ ▀█ ▐█ ▄█▐█ ▀█ •██  ██▪▐█▐█▪██▌
 * ▄█▀▀█  ██▀·▄█▀▀█  ▐█.▪██▀▐█▐█▌▐█▪
 * ▐█ ▪▐▌▐█▪·•▐█ ▪▐▌ ▐█▌·██▌▐▀ ▐█▀·.
 *  ▀  ▀ .▀    ▀  ▀  ▀▀▀ ▀▀▀ ·  ▀ •
 *  <https://fortreeforums.xyz/>
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] Activity/Longevity Meters ("ActLong").
 *
 *  ActLong is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ActLong is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ActLong.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\ActLong\Option;

use XF;
use XF\Entity\Option;
use XF\Option\AbstractOption;

class Meter extends AbstractOption
{
    /**
     * Updates the activity meter reset job to run
     * at the specified frequency.
     */
    public static function updateCronTimer(&$value, Option $option): bool
    {
        $job = XF::finder('XF:CronEntry')->where('cron_method', 'resetActivityMeter')->fetchOne();

        switch($value)
        {
            case 'day':   $runRules = ['day_type' => 'dow', 'dow' => [-1]]; break;
            case 'week':  $runRules = ['day_type' => 'dow', 'dow' => [1]];  break;
            case 'month': $runRules = ['day_type' => 'dom', 'dom' => [1]];  break;
        }

        $runRules['hours'] = [0];
        $runRules['minutes'] = [0];

        $job->run_rules = $runRules;
        $job->save(true, false);

        return true;
    }

    /**
     * Updates the forum start date used by the longevity meters
     * to the specified value.
     * 
     * Falls back to the registration date of User ID 1 if the
     * specified value is invalid.
     */
    public static function updateForumStartDate(&$value, Option $option): bool
    {
        $app = XF::app();
        $simpleCache = $app->simpleCache();

        if(!empty($value))
        {
            $value = strtotime($value);
        }
        else
        {
            $value = $app->em()->find('XF:User', 1)->register_date;
        }

        $option->option_value = $value;
        $option->save();

        $simpleCache['apathy/ActLong']['forum_start'] = $value;

        return true;
    }
}
 
