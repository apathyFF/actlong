<?php

/**
 *  ▄▄▄·  ▄▄▄· ▄▄▄· ▄▄▄▄▄ ▄ .▄ ▄· ▄▌
 * ▐█ ▀█ ▐█ ▄█▐█ ▀█ •██  ██▪▐█▐█▪██▌
 * ▄█▀▀█  ██▀·▄█▀▀█  ▐█.▪██▀▐█▐█▌▐█▪
 * ▐█ ▪▐▌▐█▪·•▐█ ▪▐▌ ▐█▌·██▌▐▀ ▐█▀·.
 *  ▀  ▀ .▀    ▀  ▀  ▀▀▀ ▀▀▀ ·  ▀ •
 *  <https://fortreeforums.xyz/>
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] Activity/Longevity Meters ("ActLong").
 *
 *  ActLong is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ActLong is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ActLong.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\ActLong\XF\Entity;

use XF;

class User extends XFCP_User
{
    public function getActivityPercentage(): int
    {
        if(!$this->canCalculateActivityPercentage())
        {
            return 0;
        }

        $activity = $this->ap_actlong_activity_meter;
        $maxPosts = $this->app()->options->apActLongActivityMaxPosts;

        $percent = ( ( $activity / $maxPosts ) * 100 );

        return min(100, round($percent));
    }

    /**
     * The "forum start date" is initially determined from
     * the registration date of $user_id 1, but can be
     * reconfigured in the addon's options
     */
    public function getLongevityPercentage(): int
    {
        if(!$this->canCalculateLongevityPercentage())
        {
            return 0;
        }

        $simpleCache = $this->app()->simpleCache();
        $time = XF::$time;

        $forumStart = ( $time - $simpleCache['apathy/ActLong']['forum_start'] );
        $registerDate = ( $time - $this->register_date );

        $percent = ( ( $registerDate / $forumStart ) * 100 );

        return min(100, round($percent));
    }

    /**
     * - Checks if Activity meters are disabled
     * - Checks if user has `apAlViewActivityMeter` permission
     */
    public function canCalculateActivityPercentage(): bool
    {
        return (
            !$this->app()->options()->apActLongActivityMeterDisabled
            && $this->hasPermission('general', 'apAlViewActivityMeter')
        );
    }

    /**
     * - Checks if Longevity meters are disabled
     * - Checks if user has `apAlViewLongevityMeter` permission
     */
    public function canCalculateLongevityPercentage(): bool
    {
        return (
            !$this->app()->options()->apActLongLongevityMeterDisabled
            && $this->hasPermission('general', 'apAlViewLongevityMeter')
        );
    }
}
